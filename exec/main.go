package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"time"

	"gitlab.com/timofeev.pavel.art/go-parser"
	"gitlab.com/timofeev.pavel.art/go-parser/database"
	"gitlab.com/timofeev.pavel.art/go-parser/handler"
	"gitlab.com/timofeev.pavel.art/go-parser/repository"
	"gitlab.com/timofeev.pavel.art/go-parser/service"

	"github.com/joho/godotenv"
	"github.com/tebeka/selenium"
)

const port = ":8080"

var (
	driver selenium.WebDriver
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Fatal(".env not found")
	}
}

func getVacancyHandler(db repository.Repository) handler.Handler {
	repo := repository.NewVacancyRepository(db)
	serv := service.NewVacancyService(repo)
	handle := handler.NewHandler(driver, serv)

	return *handle
}

func main() {
	var (
		err    error
		db     repository.Repository
		server parser.Server
	)

	db, err = database.NewDatabase()
	if err != nil {
		log.Fatal(err)
	}

	handle := getVacancyHandler(db)

	// init start driver
	if err = handle.RunDriver(); err != nil {
		log.Fatal(err)
	}
	if err = handle.QuitServer(); err != nil {
		log.Println(err)
	}

	go func() {
		if err = server.Run(handle.Routes(), port); err != nil {
			if err = handle.QuitServer(); err != nil {
				log.Println(err)
			}
			log.Fatal(err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err = handle.QuitServer(); err != nil {
		log.Println(err)
	}

	if err = server.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting.")
}
