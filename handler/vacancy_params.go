// nolint:all
package handler

// swagger:parameters vacancySearchRequest
type vacancySearchRequest struct {
	// In:query
	Query string `json:"query"`
}

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// Identificator of vacancy to delete
	//
	// in: query
	Identificator string `json:"identificator"`
}

// swagger:parameters vacancyGetRequest
type vacancyGetRequest struct {
	// Identificator of vacancy to get
	//
	// in: query
	Identificator string `json:"identificator"`
}

// swagger:parameters vacancyListRequest
type vacancyListRequest struct {
}
