package handler

import (
	"fmt"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

func (h *Handler) RunDriver() error {
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	var err error

	urlPrefix := fmt.Sprintf("http://%v:%v/wd/hub", h.endpoint, h.port)

	h.driver, err = selenium.NewRemote(caps, urlPrefix)
	if err != nil {
		return err
	}
	return nil
}

func (h *Handler) QuitServer() error {
	return h.driver.Quit()
}
