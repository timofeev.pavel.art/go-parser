package handler

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/v5/middleware"
)

func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./doc"))).ServeHTTP(w, r)
	})

	r.Post("/search", h.VacancySearch)
	r.Delete("/delete", h.VacancyDelete)
	r.Get("/get", h.VacancyGetByID)
	r.Get("/list", h.VacancyGetList)

	return r
}
