// nolint:all
package handler

import "gitlab.com/timofeev.pavel.art/go-parser/models"

// swagger:response vacancySearchResponse
type vacancySearchResponse struct {
	// in:body
	Count map[string]int `json:"count"`
}

// swagger:response vacancyDeleteResponse
type vacancyDeleteResponse struct {
}

// swagger:response vacancyGetResponse
type vacancyGetResponse struct {
	// in:body
	Body models.Vacancy
}

// swagger:response vacancyListResponse
type vacancyListResponse struct {
	// in:body
	Body []models.Vacancy
}
