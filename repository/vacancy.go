package repository

import (
	"gitlab.com/timofeev.pavel.art/go-parser/models"
)

type VacancyRepository struct {
	Handler Repository
}

func NewVacancyRepository(handler Repository) VacancyRepository {
	return VacancyRepository{Handler: handler}
}

func (vr VacancyRepository) Create(vac models.Vacancy) error {
	return vr.Handler.Create(vac)
}

func (vr VacancyRepository) GetByID(id int) (models.Vacancy, error) {
	return vr.Handler.GetByID(id)
}

func (vr VacancyRepository) GetList() ([]models.Vacancy, error) {
	return vr.Handler.GetList()
}

func (vr VacancyRepository) Delete(id int) error {
	return vr.Handler.Delete(id)
}
