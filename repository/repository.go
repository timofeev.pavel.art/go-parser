package repository

import (
	"gitlab.com/timofeev.pavel.art/go-parser/models"
)

type Repository interface {
	Create(dto models.Vacancy) error
	GetByID(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}
