package database

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/timofeev.pavel.art/go-parser/models"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDB struct {
	MongoClient mongo.Client
	db          *mongo.Database
}

func NewMongoDB(host, port, user, pwd string) (MongoDB, error) {
	var (
		mongoDB MongoDB
	)
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%v:%v@%v:%v", user, pwd, host, port))
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return mongoDB, err
	}

	mongoDB.MongoClient = *client
	mongoDB.db = client.Database("mongodb")

	_, err = mongoDB.db.Collection("vacancy").Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bson.D{{Key: "identifier", Value: 1}},
			Options: options.Index().SetUnique(true),
		})

	if err != nil {
		return MongoDB{}, err
	}
	return mongoDB, nil
}

func (m MongoDB) Create(vac models.Vacancy) error {
	collection := m.db.Collection("vacancy")
	collection.Indexes()
	_, err := collection.InsertOne(context.TODO(), vac)
	if err != nil {
		return err
	}

	return nil
}

func (m MongoDB) GetByID(id int) (models.Vacancy, error) {
	var vac models.Vacancy

	collection := m.db.Collection("vacancy")

	if err := collection.FindOne(context.TODO(), bson.M{"identifier": id}).Decode(&vac); err != nil {
		return models.Vacancy{}, err
	}

	return vac, nil
}

func (m MongoDB) GetList() ([]models.Vacancy, error) {
	var vacs []models.Vacancy

	collection := m.db.Collection("vacancy")

	curr, err := collection.Find(context.TODO(), bson.D{{}}, options.Find())
	if err != nil {
		return nil, err
	}

	for curr.Next(context.TODO()) {
		var vac models.Vacancy
		err = curr.Decode(&vac)
		if err != nil {
			return nil, err
		}
		vacs = append(vacs, vac)
	}

	return vacs, nil
}

func (m MongoDB) Delete(id int) error {
	collection := m.db.Collection("vacancy")

	_, err := collection.DeleteOne(context.TODO(), bson.M{"identifier": id})
	if err != nil {
		return err
	}

	return nil
}
