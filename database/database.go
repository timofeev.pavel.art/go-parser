package database

import (
	"fmt"
	"os"

	"gitlab.com/timofeev.pavel.art/go-parser/repository"
)

func NewDatabase() (repository.Repository, error) {
	var (
		repoInit repository.Repository
		err      error
	)
	dbType := GetEnv("DB_TYPE", "")

	switch dbType {
	case "postgres":
		repoInit, err = NewPostgresDB(
			GetEnv("DB_PSQL_HOSTNAME", ""),
			GetEnv("DB_PSQL_PORT_INT", ""),
			GetEnv("DB_PSQL_USR", ""),
			GetEnv("DB_PSQL_PWD", ""),
		)

		if err != nil {
			return nil, err
		}

	case "mongo":
		repoInit, err = NewMongoDB(
			GetEnv("DB_MONGO_HOSTNAME", ""),
			GetEnv("DB_MONGO_PORT_INT", ""),
			GetEnv("DB_MONGO_USR", ""),
			GetEnv("DB_MONGO_PWD", ""),
		)
		if err != nil {
			return nil, err
		}

	default:
		return nil, fmt.Errorf("wrong database name")
	}

	return repoInit, nil
}

func GetEnv(key, defaultVal string) string {
	if v, exist := os.LookupEnv(key); exist {
		return v
	}

	return defaultVal
}
