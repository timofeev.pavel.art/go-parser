package database

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/timofeev.pavel.art/go-parser/models"
)

type PostgresDB struct {
	Database *sqlx.DB
}

func NewPostgresDB(host, port, user, pwd string) (PostgresDB, error) {
	var postgresDB PostgresDB
	db, err := sqlx.Open("postgres",
		fmt.Sprintf("host=%s port=%s user=%s password=%s sslmode=disable dbname=postgres",
			host, port, user, pwd),
	)
	if err != nil {
		return postgresDB, err
	}

	stmt, err := db.Prepare(`CREATE TABLE IF NOT EXISTS vacancy (
										ID SERIAL PRIMARY KEY,
										IDENTIFIER INTEGER UNIQUE,
										LINK TEXT,
										TITLE CHAR(255),
										COMPANY CHAR(155),
										WEBSITE TEXT,
										DESCRIPTION TEXT,
										DATEPOST DATE                   
									)`,
	)

	if err != nil {
		return postgresDB, err
	}

	if _, err = stmt.Exec(); err != nil {
		return postgresDB, err
	}

	postgresDB.Database = db
	return postgresDB, nil
}

func (p PostgresDB) Create(vac models.Vacancy) error {
	stmt, err := p.Database.Prepare(`
			INSERT INTO vacancy (IDENTIFIER, LINK, TITLE, COMPANY, WEBSITE, DESCRIPTION, DATEPOST)
			VALUES ($1,$2,$3,$4,$5,$6,$7) 
			ON CONFLICT (identifier) DO NOTHING;`)
	if err != nil {
		return err
	}

	if _, err = stmt.Exec(vac.Identifier, vac.Link, vac.Title, vac.Company, vac.Website, vac.Description, vac.DatePost); err != nil {
		return err
	}

	return nil
}

func (p PostgresDB) GetByID(id int) (models.Vacancy, error) {
	var vac models.Vacancy
	err := p.Database.Get(&vac, `
		SELECT * FROM vacancy
		 WHERE IDENTIFIER = $1`, id)

	if err != nil {
		return models.Vacancy{}, err
	}

	return vac, nil
}

func (p PostgresDB) GetList() ([]models.Vacancy, error) {
	var vacs []models.Vacancy

	if err := p.Database.Select(&vacs, `SELECT * FROM vacancy`); err != nil {
		return nil, err
	}

	return vacs, nil
}

func (p PostgresDB) Delete(id int) error {
	_, err := p.Database.Exec(`
					DELETE FROM vacancy
					 WHERE IDENTIFIER = $1`, id)
	if err != nil {
		return err
	}

	return nil
}
