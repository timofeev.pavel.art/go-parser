package service

import (
	"gitlab.com/timofeev.pavel.art/go-parser/models"
)

type VacancyService struct {
	repository models.VacancyRepository
}

func NewVacancyService(repo models.VacancyRepository) VacancyService {
	return VacancyService{
		repository: repo,
	}
}

func (v *VacancyService) Create(dto models.Vacancy) error {
	return v.repository.Create(dto)
}

func (v *VacancyService) GetByID(id int) (models.Vacancy, error) {
	return v.repository.GetByID(id)
}

func (v *VacancyService) GetList() ([]models.Vacancy, error) {
	return v.repository.GetList()
}

func (v *VacancyService) Delete(id int) error {
	return v.repository.Delete(id)
}
