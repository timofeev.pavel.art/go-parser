package models

import (
	"encoding/json"
	"strconv"
	"time"
)

// Vacancy store info about vacancy
type Vacancy struct {
	ID          string    `json:"id" bson:"_id,omitempty"`
	Identifier  int       `json:"identifier" bson:"identifier"`
	Link        string    `json:"link" bson:"link"`
	Title       string    `json:"title" bson:"title"`
	Company     string    `json:"company" bson:"company"`
	Website     string    `json:"site" bson:"website"`
	Description string    `json:"description" bson:"description"`
	DatePost    time.Time `json:"datePost" bson:"datePost"`
}

type VacancyRepository interface {
	Create(dto Vacancy) error
	GetByID(id int) (Vacancy, error)
	GetList() ([]Vacancy, error)
	Delete(id int) error
}

func (t *Vacancy) Unmarshal(data []byte) error {
	var parse RawVacancy

	err := json.Unmarshal(data, &parse)
	if err != nil {
		return err
	}
	t.Title = parse.Title
	t.Identifier, err = strconv.Atoi(parse.Identifier.Value)
	if err != nil {
		return err
	}
	t.Company = parse.HiringOrganization.Name
	t.Website = parse.HiringOrganization.SameAs
	t.Description = parse.Description

	dateTime, err := time.Parse("2006-01-02", parse.DatePosted)
	if err != nil {
		return err
	}

	t.DatePost = dateTime
	return nil
}

// RawVacancy struct for parsing info about vacancy json
type RawVacancy struct {
	Title              string             `json:"title"`
	Description        string             `json:"description"`
	Identifier         Identifier         `json:"identifier"`
	HiringOrganization HiringOrganization `json:"hiringOrganization"`
	DatePosted         string             `json:"datePosted"`
}

type Identifier struct {
	Value string `json:"value"`
}

// HiringOrganization struct for parsing info about organization
type HiringOrganization struct {
	Name   string `json:"name"`
	SameAs string `json:"sameAs"`
}
